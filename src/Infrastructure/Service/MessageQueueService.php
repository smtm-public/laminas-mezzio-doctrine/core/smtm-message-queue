<?php

declare(strict_types=1);

namespace Smtm\MessageQueue\Infrastructure\Service;

use Smtm\Base\Infrastructure\Service\AbstractInfrastructureService;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use JetBrains\PhpStorm\Pure;
use Laminas\Mail\Address;
use Laminas\Mail\AddressList;
use Laminas\Mail\Message;
use Laminas\Mail\Transport\TransportInterface;
use Traversable;

class MessageQueueService extends AbstractInfrastructureService
{
    #[Pure] public function __construct(
        InfrastructureServicePluginManager $infrastructureServicePluginManager,
        protected array $config
    ) {
        parent::__construct($infrastructureServicePluginManager);
    }
}
