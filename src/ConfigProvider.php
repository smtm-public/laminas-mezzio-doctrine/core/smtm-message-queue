<?php

declare(strict_types=1);

namespace Smtm\MessageQueue;

use JetBrains\PhpStorm\ArrayShape;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ConfigProvider
{
    #[ArrayShape([
        'dependencies' => 'array',
        'doctrine' => 'array',
        'message_queue' => 'array'
    ])] public function __invoke(): array
    {
        return [
            'dependencies' => include __DIR__ . '/../config/dependencies.php',
            'doctrine' => include __DIR__ . '/../config/doctrine.php',
            'message_queue' => include __DIR__ . '/../config/message_queue.php',
        ];
    }
}
