<?php

declare(strict_types=1);

namespace Smtm\MessageQueue\Context\Message\Infrastructure\Repository;

use Smtm\Base\Infrastructure\Doctrine\Orm\AbstractRepository;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class MessageRepository extends AbstractRepository implements MessageRepositoryInterface
{

}
