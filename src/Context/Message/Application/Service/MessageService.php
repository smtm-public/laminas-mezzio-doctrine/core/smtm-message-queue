<?php

declare(strict_types=1);

namespace Smtm\MessageQueue\Context\Message\Application\Service;

use Smtm\Base\Application\Service\ApplicationService\DbService\AbstractUuidAwareEntityDbService;
use Smtm\MessageQueue\Context\Message\Application\Hydrator\MessageHydrator;
use Smtm\MessageQueue\Context\Message\Domain\Message;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class MessageService extends AbstractUuidAwareEntityDbService
{
    protected ?string $entityName = Message::class;
    protected ?string $hydratorName = MessageHydrator::class;
}
