<?php

declare(strict_types=1);

namespace Smtm\MessageQueue\Context\Message\Application\Hydrator;

use Smtm\Base\Application\Hydrator\DomainObjectHydrator;

class MessageHydrator extends DomainObjectHydrator
{
    /** @inheritdoc  */
    protected array $mustHydrate = [
    ];
}
