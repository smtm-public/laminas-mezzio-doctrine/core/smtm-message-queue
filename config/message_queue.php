<?php

declare(strict_types=1);

namespace Smtm\MessageQueue;

use Smtm\Base\Infrastructure\Helper\EnvHelper;

if (file_exists(__DIR__ . '/../../../../.env.smtm.smtm-message-queue')) {
    $dotenv = \Dotenv\Dotenv::createMutable(
        __DIR__ . '/../../../../', '.env.smtm.smtm-message-queue'
    );
    $dotenv->load();
}

return [
    'connections' => json_decode(EnvHelper::getEnvFromSuperGlobalOrProcess('MESSAGE_QUEUE_CONNECTIONS', '[]')),
];
