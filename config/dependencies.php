<?php

declare(strict_types=1);

namespace Smtm\MessageQueue;

use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Smtm\MessageQueue\Infrastructure\Service\EmailService;
use Smtm\MessageQueue\Infrastructure\Service\Factory\MessageQueueConfigAwareServiceFactory;
use Smtm\MessageQueue\Infrastructure\Service\MessageQueueService;
use Psr\Container\ContainerInterface;

return [
    'delegators' => [
        InfrastructureServicePluginManager::class => [
            function (
                ContainerInterface $container,
                $name,
                callable $callback,
                array $options = null
            ) {
                /** @var InfrastructureServicePluginManager $infrastructureServicePluginManager */
                $infrastructureServicePluginManager = $callback();

                return $infrastructureServicePluginManager->configure(
                    [
                        'factories' => [
                            MessageQueueService::class => MessageQueueConfigAwareServiceFactory::class,
                        ],
                    ]
                );
            }
        ],
    ],
];
