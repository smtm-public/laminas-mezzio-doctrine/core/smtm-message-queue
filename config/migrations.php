<?php

declare(strict_types=1);

namespace Smtm\MessageQueue;

return [
    'em' => 'default',

    'table_storage' => [
        'table_name' => 'doctrine_migrations_smtm_message_queue',
    ],

    'migrations_paths' => [
        'Smtm\MessageQueue\Migration' => __DIR__ . '/../data/db/migrations',
    ],
];
