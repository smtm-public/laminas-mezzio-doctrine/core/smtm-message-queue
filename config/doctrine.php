<?php

declare(strict_types=1);

namespace Smtm\MessageQueue;

return [
    'orm' => [
        'mapping' => [
            'paths' => [
                __DIR__ . '/../src/Context/Message/Infrastructure/Doctrine/Orm/Mapping',
            ],
        ],
    ],
];
