<?php

declare(strict_types=1);

namespace Smtm\MessageQueue\Migration;

use Smtm\Base\Infrastructure\Doctrine\Migration\CommonMigrationTrait;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Types;
use Doctrine\Migrations\AbstractMigration;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Version20201202120000 extends AbstractMigration
{

    use CommonMigrationTrait;

    public function up(Schema $schema): void
    {
        $this->createMessageQueueMessageTable($schema);
    }

    public function createMessageQueueMessageTable(Schema $schema): void
    {
        $messageQueueMessageTable = $schema->createTable('message_queue_message');
        $messageQueueMessageTable->addColumn('id', Types::INTEGER, ['notNull' => true])
            ->setAutoincrement(true);
        $messageQueueMessageTable->setPrimaryKey(['id']);
        $this->addUuidStringColumnAndUniqueIndex($messageQueueMessageTable);
        $messageQueueMessageTable->addColumn('subject', Types::TEXT, ['notNull' => false]);
        $this->addCreatedDatetimeImmutableColumnAndIndex($messageQueueMessageTable);
        $this->addModifiedDatetimeMutableColumnAndIndex($messageQueueMessageTable);
        $this->addNotArchivedSmallintColumnAndIndex($messageQueueMessageTable);
        $this->addArchivedDatetimeMutableColumnAndIndex($messageQueueMessageTable);
    }

    public function down(Schema $schema): void
    {
        $schema->dropTable('message_queue_message');
    }
}
